require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/activerecord'

# for work rake with ruby 2.3.0
# change file ~/.rvm/rubies/ruby-2.3.0/lib/ruby/rubygems/specification.rb
# add method
# 	def this; self; end
# in class Gem::Specification < Gem::BasicSpecification

set :database, "sqlite3:database.db"

use Rack::Session::Pool

configure do

end

before do

end

get '/' do
	erb :index
end
